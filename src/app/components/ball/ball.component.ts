import { Component, OnInit, Input, ElementRef } from '@angular/core';

@Component({
    selector: 'ball',
    templateUrl: './ball.component.html',
    styleUrls: ['./ball.component.css']
})
export class BallComponent implements OnInit {

    @Input() text: string;
    @Input() color: string;
    @Input() width: string;
    @Input() height: string;
    
    nativeElement = this.hostElement.nativeElement as HTMLElement;
    
    constructor(private hostElement: ElementRef) { }

    ngOnInit() {
        this.styling();
    }

    styling() {
        this.nativeElement.style.width = this.width + "px";
        this.nativeElement.style.height = this.height + "px";
        this.nativeElement.style.backgroundColor = this.color;
        this.nativeElement.classList.add("ball-body");
        this.nativeElement.classList.add("rounded-circle");
        this.nativeElement.classList.add("text-center");
        this.nativeElement.classList.add("text-dark");
        this.nativeElement.classList.add("d-flex");
        this.nativeElement.classList.add("justify-content-center");
        this.nativeElement.classList.add("align-items-center");
        this.nativeElement.classList.add("m-1");
    }
    
}
