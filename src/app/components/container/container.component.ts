import { Component, OnInit, Input, ElementRef } from '@angular/core';

@Component({
    selector: 'container',
    templateUrl: './container.component.html',
    styleUrls: ['./container.component.css']
})
export class ContainerComponent implements OnInit {

    @Input() width: number;
    @Input() height: number;
    @Input() x: number = 0;
    @Input() y: number = 0;
    @Input() position: string;
    @Input() centered: string = "";

    nativeElement = this.hostElement.nativeElement as HTMLElement;

    constructor(private hostElement: ElementRef) { }

    ngOnInit() {
        this.styling();
    }

    styling() {

        this.nativeElement.setAttribute("style", 
            "width: " + this.width + "px;" +  
            "height: " + this.height + "px;" +
            "left: " + this.x  + "px;" + 
            "top: " + this.y + "px;" +     
            "position: " + this.position + ";" + 
            "opacity: 1 !important;"
        );

        if(this.centered == "true") {
            this.nativeElement.style.left = document.documentElement.clientWidth/2 - this.width/2 + "px";
            this.nativeElement.style.top = document.documentElement.clientHeight/2 - this.height/2 + "px";
        }
    }
}
