import { Injectable } from '@angular/core';

@Injectable()
export class UtilsService {

    constructor() { }

    disable(elementId) {
        $("#" + elementId).attr("disabled", "");
    }

    clamp(val, min, max) {
        return Math.min(Math.max(min,val),max);
    }
}
