import { TestBed, inject } from '@angular/core/testing';

import { BallselectionService } from './ballselection.service';

describe('BallselectionService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BallselectionService]
    });
  });

  it('should be created', inject([BallselectionService], (service: BallselectionService) => {
    expect(service).toBeTruthy();
  }));
});
