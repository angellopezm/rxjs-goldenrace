import { Injectable } from '@angular/core';

@Injectable()
export class BallselectionService {

    ballPool: Array<HTMLElement> = null;
    selectedPool: Array<HTMLElement> = new Array();
    selected: HTMLElement = null;

    constructor() { 
        this.ballPool = <any>document.getElementsByClassName("selectable");
    }

    // Local update function
    update() {
        this.styling();
    }

    select(element) {
        if(this.selected == element) {
            this.selected = null;
        } else {
            this.selected = element;
        }
    }

    addToSelectedPool() {
        this.selectedPool.push(this.selected);
    }

    styling() {
        if (this.selected != null) {
            this.selected.classList.add("selected");
        }

        for (let i = 0; i < this.ballPool.length; i++) {
            if (this.selected != this.ballPool[i]) {
                this.ballPool[i].classList.remove("selected");
            }
        }
    }

}
