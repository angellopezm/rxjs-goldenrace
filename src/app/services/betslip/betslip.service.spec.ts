import { TestBed, inject } from '@angular/core/testing';

import { BetslipService } from './betslip.service';

describe('BetslipService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BetslipService]
    });
  });

  it('should be created', inject([BetslipService], (service: BetslipService) => {
    expect(service).toBeTruthy();
  }));
});
