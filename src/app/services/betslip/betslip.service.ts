import { Injectable, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { BallselectionService } from '../ballselection/ballselection.service';
import { UtilsService } from '../utils/utils.service';
import * as $ from 'jquery';

@Injectable()
export class BetslipService {

    turn = 1;
    total = 0;
    betQuantity: number;
    message = "";
    blockUserInput = false;
    rollMessage = "";
    
    constructor(
        private ballSelectionService: BallselectionService, 
        private utilsService: UtilsService
    ) { }

    start() {
        $("#moneytray").val(5);
        $("#window-over").css("display", "none");
        this.betQuantity = parseInt($("#moneytray").val().toString());
    }

    update() {
        $("#betButton").attr("disabled", "");
        this.message = "You can place your bet now";

        if(!$("#moneytray").attr("disabled")) {
            this.message = "Select the quantity you want to spend, then confirm by clicking 'Ok'";
        } else if(this.ballSelectionService.selected == null) { 
            this.message = "Select the ball you want to bet for";
        } else {
            $("#betButton").removeAttr("disabled");
        }
    }

    bet() {
        let machineBall = null;
        $("#window-over").css("display", "inline-block");
        $("#confirmButton").css("display", "none");

        Observable.timer(0).subscribe(() => { // A simple delay for the animation to work
            this.blockUserInput = true;
            this.rollMessage = "ROLLING...";
        });

        $("#selected").css("background-color", this.ballSelectionService.selected.style.backgroundColor);

        let randomizer = Observable.interval(100).subscribe(() => {
            let rand = Math.floor(Math.random()*this.ballSelectionService.ballPool.length);
            $("#machine").css("background-color", this.ballSelectionService.ballPool[rand].style.backgroundColor);
            $("#machine").children()[0].innerHTML = this.ballSelectionService.ballPool[rand].children[0].innerHTML;
            machineBall = this.ballSelectionService.ballPool[rand];
        });

        Observable.timer(5000).subscribe(() => { // A simple delay for the animation to work
            randomizer.unsubscribe();

            $("#confirmButton").css("display", "inline-block");
            if(this.ballSelectionService.selected.getAttribute("text") == machineBall.children[0].innerHTML) {
                let profit = (this.betQuantity * 1.5);
                this.total+= profit;
                this.rollMessage = "YOU WON! PROFIT: " + profit + " €";
            } else {
                this.rollMessage = "YOU LOSE";
            }
            
        });
    }

    confirm() {
        this.turn++;
        this.ballSelectionService.selectedPool.push(this.ballSelectionService.selected);
        $("#moneytray").removeAttr("disabled");
        this.blockUserInput = false;
        $("#window-over").css("display", "none");
    }

    clampValue() {
        if($("#moneytray").val() < 5) {
            $("#moneytray").val(this.utilsService.clamp(this.betQuantity, 5, Number.POSITIVE_INFINITY));
        }
    }
}
