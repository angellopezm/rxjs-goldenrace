import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { BallComponent } from './components/ball/ball.component';
import { ContainerComponent } from './components/container/container.component';
import { BallselectionService} from './services/ballselection/ballselection.service';
import { BetslipService } from './services/betslip/betslip.service';
import { UtilsService } from './services/utils/utils.service';
 

@NgModule({
    declarations: [
        AppComponent,
        BallComponent,
        ContainerComponent,
    ],
    imports: [
        BrowserModule
    ],
    providers: [
        BallselectionService,
        BetslipService, 
        UtilsService
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
