import { Component, OnInit, Injector } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { BallselectionService } from './services/ballselection/ballselection.service';
import { BetslipService } from './services/betslip/betslip.service';
import { UtilsService } from './services/utils/utils.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

    constructor(
        private ballSelectionService: BallselectionService,
        private betSlipService: BetslipService,
        private utilsService: UtilsService
    ) { }
    
    // Global start function
    ngOnInit() { 
        this.betSlipService.start();
    }

    // Global update function
    update = Observable.interval(0).subscribe(() => { 
        this.ballSelectionService.update();
        this.betSlipService.update();
    });
    
}
