import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { BallComponent } from './components/ball/ball.component';
import { ContainerComponent } from './components/container/container.component';
import { BallselectionService } from './services/ballselection/ballselection.service';
import { BetslipService } from './services/betslip/betslip.service';
import { UtilsService } from './services/utils/utils.service';

describe('AppComponent', () => {
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                AppComponent,
                BallComponent,
                ContainerComponent
            ],
            providers: [
                BallselectionService,
                BetslipService,
                UtilsService
            ],
        }).compileComponents();
    }));
    it('should create the app', async(() => {
        const fixture = TestBed.createComponent(AppComponent);
        const app = fixture.debugElement.componentInstance;
        expect(app).toBeTruthy();
    }));

    it('should have the "ngOnInit" and "update" functions', async(() => {
        let fixture = TestBed.createComponent(AppComponent);
        let app = fixture.debugElement.componentInstance;
        spyOn(app, 'ngOnInit');
        spyOn(app, 'update');
    }));

});
